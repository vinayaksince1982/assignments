import json
import bcrypt


class EmployeeModel(object):
    def __init__(self):
        self.user_name = None
        self.real_name = None
        self._password = None
        self.phone_number = None
        self.revision = 0
        self.employee_type = "normal"
        self.is_authentic = False

    __slots__ = ["user_name", "real_name", "phone_number", "_password",
                 "revision", "employee_type", "is_authentic"]

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, plain_text_password):
        self._password = bcrypt.hashpw(plain_text_password, bcrypt.gensalt())

    def authenticate(self, plain_test_password):
        self.is_authentic = bcrypt.checkpw(plain_test_password, self.password)


class ModelOperations(object):
    JSON_DB_FILE = "employee_db.json"

    @classmethod
    def add(cls, payload):
        data = cls.get_all()
        username = payload["username"]
        if username in data:
            return None
        new_employee = EmployeeModel()
        for key in payload.keys():
            new_employee[key] = payload[key]
        data[username] = new_employee
        cls.write_to_file(data)
        return

    @classmethod
    def get(cls, username):
        # TODO: Since this is file, need to serialize access to it
        with open(cls.JSON_DB_FILE) as fp:
            data = json.load(fp)
            if username not in data:
                return None
            del data[username]['password']
            return data[username]

    @classmethod
    def get_all(cls):
        # TODO: Since this is file, need to serialize access to it
        with open(cls.JSON_DB_FILE) as fp:
            data = json.load(fp)
            for username in data:
                del data[username]['password']
            return data

    @classmethod
    def write_to_file(cls, data):
        output = json.dumps(data)
        with open(cls.JSON_DB_FILE, "w") as fp:
            fp.write(output)

    @classmethod
    def update(cls, username, payload):
        # TODO: Since this is file, need to serialize access to it
        data = cls.get_all()
        if username not in data:
            return None
        if payload['revision'] != data[username]['revision']:
            raise BadRevision()
        for key in payload.keys():
            data[username][key] = payload[key]
        data[username]['revision'] += 1
        cls.write_to_file(data)
        del data[username]['password']
        return data[username]

    @classmethod
    def delete(cls, username):
        # TODO: Since this is file, need to serialize access to it
        data = cls.get_all()
        if username not in data:
            return
        del data[username]
        cls.write_to_file(data)


class BadRevision(Exception):
    pass
