""" Create the blueprint used by rest api here."""

from flask import Blueprint

bp = Blueprint('api', __name__)     # pylint: disable=invalid-name

# pylint: disable=wrong-import-position

from employees.api import views

# pylint: enable=wrong-import-position
