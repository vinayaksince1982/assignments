"""
Views for the employee rest-apis go here.
"""
from flask import g
from flask_restful import Resource, reqparse, Api
from flask_httpauth import HTTPBasicAuth
from employees.api.models import ModelOperations, BadRevision
from employees.api import bp

auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(username, password):
    user = ModelOperations.get(username)
    if not user or not user.authenticate(password):
        return False
    g.user = user
    return True


class EmployeesListView(Resource):
    """
    Class for the /employees endpoint
    """
    @auth.login_required
    def post(self):
        """ Parses post request and creates an employee in database """
        if g.user.employee_type not in ('manager', 'admin'):
            return "Forbidden", 403
        parser = reqparse.RequestParser()
        parser.add_argument('user_name', type=str, required=True)
        parser.add_argument('real_name', type=str, required=True)
        parser.add_argument('phone_number', type=str, required=True)
        parser.add_argument('password', type=str, default="default")
        parser.add_argument('employee_type', type=str, default='regular')
        args = parser.parse_args()
        payload = {'user_name': args.user_id,
                   'real_name': args.real_name,
                   'phone_number': args.phone_number,
                   'password': args.password,
                   'employee_type': args.employee_type}

        employee = ModelOperations.add(payload)
        if employee is None:
            return "Duplicate entry", 400
        return employee, 201

    @auth.login_required
    def get(self):
        """
        List all employees
        """
        if g.user.employee_type not in ('manager', 'admin'):
            return "Forbidden", 403
        employee_list = ModelOperations.get_all()
        return employee_list, 200


class EmployeesView(Resource):
    @auth.login_required
    def get(self, username):
        """
        List all employees
        """
        if g.user.employee_type not in ('manager', 'admin'):
            return "Forbidden", 403
        employee = ModelOperations.get(username)
        return employee, 200

    @auth.login_required
    def delete(self, username):
        """
        Queries for the given username, deletes if found.
        """
        if g.user.employee_type not in ('manager', 'admin'):
            return "Forbidden", 403
        ModelOperations.delete(username)
        return 200

    @auth.login_required
    def update(self, username):
        if g.user.employee_type not in ('manager', 'admin'):
            return "Forbidden", 403
        parser = reqparse.RequestParser()
        parser.add_argument('user_name', type=str)
        parser.add_argument('real_name', type=str)
        parser.add_argument('phone_number', type=str)
        parser.add_argument('employee_type', type=str,
                            choice=('regular', 'manager', 'admin'))
        parser.add_argument('revision', type=int)
        args = parser.parse_args()
        payload = {}
        if 'user_name' in args and args.user_name is not None:
            payload['user_name'] = args.user_name
        if 'real_name' in args and args.real_name is not None:
            payload['real_name'] = args.real_name
        if 'phone_number' in args and args.phone_number is not None:
            payload['phone_number'] = args.phone_number
        if 'employee_type' in args and args.employee_type is not None:
            payload['employee_type'] = args.employee_type
        if 'revision' in args and args.revision is not None:
            payload['revision'] = args.revision
        try:
            employee = ModelOperations.update(username, payload)
        except BadRevision as e:
            return "Incorrect revision specified", 400
        return employee, 200


class RegularEmployeeView(Resource):
    """
    Class for the update and change password for the employees.
    """

    @auth.login_required
    def get(self):
        """
        Queries for the profile of the logged in user, and returns it.
        """
        return g.user, 200

    @auth.login_required
    def put(self):
        """
        Queries for the given tweet_id, joins on the user table before
        returning a json response with the tweet data.
        """
        parser = reqparse.RequestParser()
        parser.add_argument('user_name', type=str)
        parser.add_argument('real_name', type=str)
        parser.add_argument('phone_number', type=str)
        parser.add_argument('revision', type=int)
        args = parser.parse_args()
        payload = {}
        if 'user_name' in args and args.user_name is not None:
            payload['user_name'] = args.user_name
        if 'real_name' in args and args.real_name is not None:
            payload['real_name'] = args.real_name
        if 'phone_number' in args and args.phone_number is not None:
            payload['phone_number'] = args.phone_number
        if 'revision' in args and args.revision is not None:
            payload['revision'] = args.revision
        try:
            employee = ModelOperations.update(g.user.username, payload)
        except BadRevision as e:
            return "Incorrect revision specified", 400
        return employee, 200


class ChangePasswordView(Resource):
    @auth.login_required
    def put(self):
        parser = reqparse.RequestParser()
        parser.add_argument('password', type=str, required=True)
        args = parser.parse_args()
        payload = {'password': args.password}
        employee = ModelOperations.update(g.user.username, payload)
        return employee, 200


api_inst = Api(bp)
api_inst.add_resource(EmployeesListView, '/employees')
api_inst.add_resource(EmployeesView, '/employees/<username>')
api_inst.add_resource(RegularEmployeeView, '/employees/profile')
api_inst.add_resource(ChangePasswordView, '/employees/change-password')
