"""
Entry point for the tweet app.
"""
import os
from flask import Flask

FLASK_CONFIG = os.getenv('FLASK_CONFIG', 'config_dev')


def get_app():
    """
    Application factory, returns a flask application object based on
    the FLASK_CONFIG environment variable.
    :return: flask app object
    """
    app = Flask(__name__)
    app.config.from_object(FLASK_CONFIG)
    # Import a module / component using its blueprint handler variable (mod_auth)
    from employees.api import bp as apis_bp
    app.register_blueprint(apis_bp, url_prefix='/api/v1')
    return app
