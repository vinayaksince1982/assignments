from employees import get_app


if __name__ == "__main__":
    app = get_app()                         # pylint: disable=invalid-name
    app.run(host='127.0.0.1', port=8080)
