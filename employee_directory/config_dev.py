"""
Add/Edit configuration for flask, deployment in dev environment
"""

THREADS_PER_PAGE = 2
CSRF_ENABLED = True
CSRF_SESSION_KEY = "Th1s 1s a s3cr3t"
# Secret key for signing cookies
SECRET_KEY = "D0nt t311 any0n3"
DEBUG = True

JSON_DB_FILE = "employee_db.json"

