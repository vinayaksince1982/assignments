# Schema
    Users:
    - user_name
    - password
    - real_name
    - phone_number
    - employee_type
    - version 

# Authentication theme
Basic Auth.

Every api will need username and password to be passed in the request headers.

# REST API endpoints
#### Create user

Only admin/managers will be able to execute this api.

    POST /api/v1/users
    Sample payload:
    {
        "user_name": "vinmal",
        "password": "default",
        "real_name": "Vinayak Mali",
        "phone_number": "9876543210",
        "employee_type": "manager"
    }
    
#### List users

Only admin/managers will be able to list all users.

    GET /api/v1/users
 
#### Get user

Only admin/managers will be list a specific user.

    GET /api/v1/users/<user_name>

#### Get user profile

Any body can execute this api. Returns attributes for the current user.

    GET /api/v1/users/profile

#### Update user profile(user version)

    UPDATE /api/v1/users
    Sample payload:
    {
        "user_name": "vinmal",
        "real_name": "Vinayak Mali",
        "phone_number": "9876543210"
    }

#### Update user profile(manager version)

Only admin/managers will have access to this api. Updates can be made on all
fields except for the password.

    PUT /api/v1/users/<user_name>
    Sample payload:
    {
        "user_name": "vinmal",
        "real_name": "Vinayak Mali",
        "phone_number": "9876543210",
        "revision": 1
    }

#### Update password

Password will be updated for the user whose user_name is in the request headers.

    PUT /api/v1/users/update-password
    Sample payload:
    {
        "password": "new_password",
        "revision": "1"
    }

#### Update password

Only admin/manager will have access to this api.

    DELETE /api/v1/users/<user_name>